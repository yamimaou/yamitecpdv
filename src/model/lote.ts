export class Lote {
    productitem_id: number;
    productlote_price: string;
    productlote_qtd: string;
    productlote_validity: number;
    productlote_waranty: number;
    productlote_code: string;
    created_at : Date;
    updated_at : Date;
  }