export class Organizacao {
    org_id: number;
    user_id: number;
    org_companyname: string;
    org_fantasyname: string;
    org_cnpj: string;
    org_ie: number
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    users: [];
    clients: [];    
  }