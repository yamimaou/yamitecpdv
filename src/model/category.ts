export class Category {
    category_id: number;
    category_name: string;
    category_type: number;
    category_status: number;
    user_id: number;
    created_at: Date;
    updated_at: Date;
    users: [];
    products: [];
  }