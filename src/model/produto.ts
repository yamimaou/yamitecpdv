export class Produto {
    product_id: number;
    category_id: number = 0;
    client_id: number;
    user_id: number;
    product_name: string;
    product_provider: string;
    product_description: string;
    product_price: number;
    product_sample: number = 0;
    product_code: string;
    product_waranty: string;
    product_weight: number;
    product_scale: string;
    created_at: Date;
    updated_at: Date;
    users: [];
    lotes: [];
    movta: [];
    clients: [];    
  }