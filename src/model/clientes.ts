export class Clientes {
    client_id: number;
    user_id: number;
    org_id: number;
    client_companyname: string;
    client_fantasyname: string;
    client_cnpj: string;
    client_ie: number
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    users: [];
    clients: [];    
  }