import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-org-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  orgs : any;
  columnsNames: string[] = [ 'RAZÃO SOCIAL', 'NOME FANTASIA', 'CNPJ','IE', 'CADASTRO', 'AÇÃO'];
  dataColumns: string[] = [ 'org_companyname', 'org_fantasyname', 'org_cnpj', 'org_ie', 'created_at','acao'];
  isLoadingResults = true;
  
  constructor(private _api: ApiService) { }
  ngOnInit() {
    this._api.getOrgs().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.orgs = response.data;
      this.isLoadingResults = false;
    }, err => {
      console.error(err);
       this.isLoadingResults = false;
    });
  }
  applyFilter(filterValue: string, formGroup : any) {
    formGroup.filter = filterValue.trim().toLowerCase();
  }
}
