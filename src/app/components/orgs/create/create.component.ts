import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators, ValidationErrors } from '@angular/forms';
import { ApiService } from '../../../services/api.service';
import { Produto } from 'src/model/produto';
import { Category } from 'src/model/category';
import { MatSnackBar, MatSnackBarRef, MatTableDataSource } from '@angular/material';
import { Lote } from 'src/model/lote';
//const productformData = require('src/app/components/data/productformData.json');
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  orgs: Produto;
  orgsForm: FormGroup;
  clientesColumns: string[] = [ 'companyname', 'fantasyname', 'cnpj', 'ie', 'created','acao'];
  clientes = new MatTableDataSource<Element[]>();
  clientsForm: FormGroup;
  isLoadingResults = false;

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.orgsForm = this.formBuilder.group({
      //'product_id': [1],
      'org_companyname': ['', [Validators.required, Validators.minLength(4)]],
      'org_fantasyname': ['', [Validators.required, Validators.minLength(2)]],
      'org_cnpj': ['', [Validators.required, Validators.minLength(10)]],
      'org_ie': ['0000-0000-0000-0000'],
    });
    this.clientsForm = this.formBuilder.group({
      'org_id': [1],
      'client_companyname': ['', [Validators.required, Validators.minLength(4)]],
      'client_fantasyname': ['', [Validators.required, Validators.minLength(2)]],
      'client_cnpj': ['', [Validators.required, Validators.minLength(10)]],
      'client_ie': ['0000-0000-0000-0000'],
    });
  }
  addOrg(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addOrg(form)
      .subscribe(res => {
        let response = JSON.parse(JSON.stringify(res));
        console.log(response.lastId);
        this.clientsForm.controls['org_id'].setValue(response.lastId);
        this.isLoadingResults = false;
        this.orgsForm.reset();
        this.snackBar.open("Organização Adicionada.",null, {
          duration : 2000,
        });
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao adicionar Organização.",null, {
          duration : 2000,
        });
        this.isLoadingResults = false;
      });
    }
    addCliente(form: NgForm) {
      this.isLoadingResults = true;
      this.api.addCliente(form)
        .subscribe(res => {
          let response = JSON.parse(JSON.stringify(res));
          console.log(response.lastId);
          let nwcliente = this.clientes.data;
          nwcliente.push(this.clientsForm.value);
          this.clientes.data = nwcliente;
          this.isLoadingResults = false;
          this.clientsForm.controls['client_fantasyname'].setValue(null);
          this.clientsForm.controls['client_companyname'].setValue(null);
          this.clientsForm.controls['client_cnpj'].setValue(null);
          this.clientsForm.controls['client_ie'].setValue(null);
          this.clientsForm.reset();
          this.clientsForm.updateValueAndValidity();
          this.snackBar.open("Cliente Adicionado à Organização.",null, {
            duration : 2000,
          });
        }, (err) => {
          console.log(err);
          this.snackBar.open("Problema ao adicionar Cliente.",null, {
            duration : 2000,
          });
          this.isLoadingResults = false;
        });
    }
  getFormValidationErrors() {
    Object.keys(this.orgsForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.orgsForm.get(key).errors;
      const translateErrors = {
        'required': ' campo Obrigatório',
        'minLength': ' minimo de caracteres não alcançado',
        'maxLength': ' máximo de caracteres atingido/ultrapassado'
      }

      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          this.snackBar.open('O Campo \"' + key + "\" apresentou o seguinte problema : " + translateErrors[keyError], null, {
            duration: 5000,
          });
        });
      }
    });
  }
  applyFilter(filterValue: string, formGroup: any) {
    formGroup.filter = filterValue.trim().toLowerCase();
  }
}
