import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { Produto } from 'src/model/produto';
import { DeleteDialogComponent } from 'src/app/components/dialogs/delete-dialog/delete-dialog.component';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  title : 'deseja ?';
  orgs: any;
  columnsNames: string[] = [ 'RAZÃO SOCIAL', 'NOME FANTASIA', 'CNPJ','IE', 'CADASTRO'];
  dataColumns: string[] = [ 'org_companyname', 'org_fantasyname', 'org_cnpj', 'org_ie', 'created_at'];
  dataVer = {}
  isLoadingResults = true;
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private deleteDialog : DeleteDialogComponent,public snackBar: MatSnackBar) { }


  ngOnInit() {
    this.getOrgs(this.route.snapshot.params['id']);
  }

  getDialog(id){
    this.deleteDialog.title = "Excluír Registro";
    this.deleteDialog.message = "Deseja Realmente Excluír o registro?"
    this.deleteDialog.openDialog(() => {
      this.deleteOrg(id);
      this.router.navigate(['/organizacoes']);
    });

  }
  getOrgs(id) {
    this.api.getOrg(id)
      .subscribe(data => {
        let response = JSON.parse(JSON.stringify(data));
        if(response.length == 0){
          this.isLoadingResults = false;
          this.orgs = response;
          console.log(this.orgs);
        return this.orgs;
        }
        this.orgs = response.data;
        console.log(this.orgs);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  deleteOrg(id) {
    this.isLoadingResults = true;
    this.api.deleteOrg(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.snackBar.open("Organização Excluída com sucesso.",null, {
            duration : 2000,
          });
          this.router.navigate(['organizacoes']);
        }, (err) => {
          console.log(err);
          this.snackBar.open(err,null, {
            duration : 2000,
          });
          this.isLoadingResults = false;
        }
      );
  }
}