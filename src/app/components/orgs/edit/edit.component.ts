import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators, ValidationErrors } from '@angular/forms';
import { ApiService } from '../../../services/api.service';
import { Organizacao } from 'src/model/organizacao';
import { Category } from 'src/model/category';
import { MatSnackBar, MatSnackBarRef, MatTableDataSource } from '@angular/material';
import { Lote } from 'src/model/lote';

@Component({
  selector: 'app-create',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  org: Organizacao;
   _id : number;
  orgForm: FormGroup;
  isLoadingResults = true;

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder, public snackBar: MatSnackBar) { 
   
  }

  ngOnInit() {
    this._id = this.route.snapshot.params['id'];
    this.getOrg(this._id);
  }
  getOrg(id) {
    this.api.getOrg(id)
      .subscribe(data => {
        let response = JSON.parse(JSON.stringify(data))
        this.org = response.data;
        if(response.length == 0){
          this.isLoadingResults = false;
          this.org = response;
          console.log(this.org);
        return this.org;
        }
        this.orgForm = this.formBuilder.group({
          'org_companyname': [this.org.org_companyname, [Validators.required, Validators.minLength(2)]],
          'org_fantasyname': [this.org.org_fantasyname, [Validators.required, Validators.minLength(2)]],
          'org_cnpj': [this.org.org_cnpj, [Validators.required, Validators.minLength(10)]],
          'org_ie': [this.org.org_ie],
        });
        console.log(this.org);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  EditOrg(form: NgForm) {
    this.isLoadingResults = true;
    this.api.updateOrg(this._id,form)
      .subscribe(res => {
        this.isLoadingResults = false;
        this.orgForm.reset();
        this.snackBar.open("Organização Atualizada com sucesso.",null, {
          duration : 2000,
        });
        this.router.navigate(['/organizacoes/'+this._id]);
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao atualizar organizacao.",null, {
          duration : 2000,
        });
        this.isLoadingResults = false;
      });
  }
  getFormValidationErrors() {
    Object.keys(this.orgForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.orgForm.get(key).errors;
      const translateErrors = {
        'required': ' campo Obrigatório',
        'minLength': ' minimo de caracteres não alcançado',
        'maxLength': ' máximo de caracteres atingido/ultrapassado'
      }

      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          this.snackBar.open('O Campo \"' + key + "\" apresentou o seguinte problema : " + translateErrors[keyError], null, {
            duration: 5000,
          });
        });
      }
    });
  }
  applyFilter(filterValue: string, formGroup: any) {
    formGroup.filter = filterValue.trim().toLowerCase();
  }
}
