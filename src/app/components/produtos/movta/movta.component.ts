import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder, NgForm } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { MatSnackBar, MatTableDataSource } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Produto } from 'src/model/produto';

@Component({
  selector: 'app-movta',
  templateUrl: './movta.component.html',
  styleUrls: ['./movta.component.css']
})
export class MovtaComponent implements OnInit {
  clientsFrom : [];
  clientsTo : [];
  unitiesFrom : any;
  unitiesTo : any;
  loteForm: FormGroup;
  isLoadingResults = false;
  produto: Produto = { 
    category_id : 0,
    user_id: 0,
    client_id : 0,
    product_id: 0,
    product_name: '',
    product_provider: '',
    product_description: '',
    product_price: 0,
    product_sample: 0,
    product_code: '',
    product_waranty: '',
    product_weight: 0,
    product_scale: '',
    created_at: null,
    updated_at: null,
    users: [],
    lotes: [],
    movta: [],
    clients: [],
  };
  movtaColumns: string[] = ['origem','destino', 'qtd', 'user','type'];
  movta = new MatTableDataSource<Element[]>();
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder, public snackBar: MatSnackBar) { }

  ngOnInit() {
    const _id = this.route.snapshot.params['id'];
    this.getProduto(_id);
    this.loteForm = this.formBuilder.group({
      'product_id': [_id],
      'client_id': [0],
      'unity_id': [0],
      'productmovta_client_id': [0],
      'productmovta_unity_id':[0],
      'productmovta_qtd': [null, [Validators.required]],
      'productmovta_type': [null, [Validators.required]],
      'productmovta_obs': [null, Validators.required]
    });
    /**** */

    this.api.getClients().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.clientsFrom = response.data;
      this.clientsTo = response.data;
      console.log(this.clientsFrom);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
  setUnitiesFrom(id: number){
    this.api.getClient(id).subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.unitiesFrom = response.data.unities;
      console.log(this.unitiesFrom);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
    
  }
  setUnitiesTo(id: number){
    this.api.getClient(id).subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.unitiesTo = response.data.unities;
      console.log(this.unitiesTo);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
  getProduto(id) {
    this.api.getProduto(id)
      .subscribe(data => {
        let response = JSON.parse(JSON.stringify(data))
        this.produto = response.data;
        console.log(this.produto);
        this.isLoadingResults = false;
      });
  }
  addMovta(form: NgForm) {
    this.isLoadingResults = true;
    console.log(form);
    //return;
    this.api.addMovta(form)
      .subscribe(res => {
        let response = JSON.parse(JSON.stringify(res));
        console.log(response.lastId);
        let nwLote = this.movta.data;
        nwLote.push(this.loteForm.value);
        this.movta.data = nwLote;
        this.isLoadingResults = false;
        this.loteForm.controls['productmovta_qtd'].setValue(1);
        this.loteForm.controls['productmovta_type'].setValue(null);
        this.loteForm.controls['productmovta_obs'].setValue(null);
        this.loteForm.reset();
        this.loteForm.updateValueAndValidity();
      }, (err) => {
        console.log(err);
        this.snackBar.open(err);
        this.isLoadingResults = false;
      });
  }

}

