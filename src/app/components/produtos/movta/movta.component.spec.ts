import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovtaComponent } from './movta.component';

describe('MovtaComponent', () => {
  let component: MovtaComponent;
  let fixture: ComponentFixture<MovtaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovtaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovtaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
