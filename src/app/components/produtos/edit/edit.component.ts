import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators, ValidationErrors } from '@angular/forms';
import { ApiService } from '../../../services/api.service';
import { Produto } from 'src/model/produto';
import { Category } from 'src/model/category';
import { MatSnackBar, MatSnackBarRef, MatTableDataSource } from '@angular/material';
import { Lote } from 'src/model/lote';

@Component({
  selector: 'app-create',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  produto: Produto;
  categories: Category;
   _id : number;
  lotes = new MatTableDataSource<Element[]>();
  lotesColumns: string[] = ['code', 'price', 'qtd', 'waranty', 'validity'];
  productForm: FormGroup;
  loteForm: FormGroup;
  isLoadingResults = true;
  dataformCampos = [];

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder, public snackBar: MatSnackBar) { 
   
  }

  ngOnInit() {
    this._id = this.route.snapshot.params['id'];
    this.getProduto(this._id);    
    this.api.getCategories().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.categories = response.data.map((v) => {
        return { name: v.category_name, value: v.category_id } 
      });
      console.log(this.categories);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
  testparam(param){
    console.log(param);
  }
  getProduto(id) {
    this.api.getProduto(id)
      .subscribe(data => {
        let response = JSON.parse(JSON.stringify(data))
        this.produto = response.data;
        if(response.length == 0){
          this.produto = response;
          console.log(this.produto);
        return this.produto;
        }
        this.dataformCampos = [
          {
            colname: 'product_id',
            coltype: 'hidden',
            collabel: 'id',
            colvalue: this.produto.product_id,
            colattr : null,
            coldata: []
          },
          {
            colname: 'category_id',
            coltype: 'select',
            collabel: 'Categoria',
            colvalue: this.produto.category_id,
            colattr : null,
            coldata: this.categories
          },
          {
            colname: 'product_name',
            coltype: 'text',
            collabel: 'NOME',
            colvalue: this.produto.product_name,
            colvalidators: [Validators.required, Validators.minLength(4)],
            colattr : null,
            coldata: [
              { name: 'teste', value: 5 },
              { name: 'teste 1', value: 2 },
              { name: 'teste 2', value: 3 },
            ]
          },
          {
            colname: 'product_provider',
            coltype: 'text',
            collabel: 'FORNECEDOR',
            colvalue: this.produto.product_provider,
            colvalidators: [Validators.required, Validators.minLength(4)],
            colattr : null,
            coldata: [
              { name: 'teste', value: 5 },
              { name: 'teste 1', value: 2 },
              { name: 'teste 2', value: 3 },
            ]
          },
          {
            colname: 'product_description',
            coltype: 'textarea',
            collabel: 'DESCRIÇÃO',
            colvalue: this.produto.product_description,
            colvalidators: [Validators.required, Validators.minLength(10)],
            colattr : null,
            coldata: [
              { name: 'teste', value: 5 },
              { name: 'teste 1', value: 2 },
              { name: 'teste 2', value: 3 },
            ]
          },
          {
            colname: 'product_price',
            coltype: 'text',
            collabel: 'PREÇO',
            colvalue: this.produto.product_price,
            colvalidators: Validators.required,
            colattr : null,
            coldata: [
              { name: 'teste', value: 5 },
              { name: 'teste 1', value: 2 },
              { name: 'teste 2', value: 3 },
            ]
          },
          {
            colname: 'product_code',
            coltype: 'text',
            collabel: 'CÓDIGO',
            colvalue: this.produto.product_code,
            colvalidators: [Validators.required, Validators.minLength(4)],
            colattr : null,
            coldata: [
              { name: 'teste', value: 5 },
              { name: 'teste 1', value: 2 },
              { name: 'teste 2', value: 3 },
            ]
          },
          {
            colname: 'product_sample',
            coltype: 'radio',
            collabel: 'PRODUTO DE AMOSTRA',
            colvalue: this.produto.product_sample,
            colattr : null,
            coldata: [
              { name: 'SIM', value: 1 },
              { name: 'NÃO', value: 0 }
            ]
          },
          {
            colname: 'product_weight',
            coltype: 'text',
            collabel: 'PESO',
            colvalue: this.produto.product_weight,
            colattr : null,
            coldata: null
          },
          {
            colname: 'product_scale',
            coltype: 'text',
            collabel: 'TAMANHO',
            colvalue: this.produto.product_scale,
            colattr : null,
            coldata: null
          },
          {
            colname: 'product_waranty',
            coltype: 'date',
            collabel: 'GARANTIA',
            colvalue: this.produto.product_waranty,
            colattr : null,
            coldata: null
          },
        ]
        console.log(this.produto);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  EditProduto(form: NgForm) {
    this.isLoadingResults = true;
    this.api.updateProduto(this._id,form)
      .subscribe(res => {
        this.isLoadingResults = false;
        this.snackBar.open("Produto Atualizado com sucesso.",null, {
          duration : 2000,
        });
        this.router.navigate(['/produtos/'+this._id]);
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao atualizar produto.",null, {
          duration : 2000,
        });
        this.isLoadingResults = false;
      });
  }
  getFormValidationErrors() {
    Object.keys(this.productForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.productForm.get(key).errors;
      const translateErrors = {
        'required': ' campo Obrigatório',
        'minLength': ' minimo de caracteres não alcançado',
        'maxLength': ' máximo de caracteres atingido/ultrapassado'
      }

      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          this.snackBar.open('O Campo \"' + key + "\" apresentou o seguinte problema : " + translateErrors[keyError], null, {
            duration: 5000,
          });
        });
      }
    });
  }
  applyFilter(filterValue: string, formGroup: any) {
    formGroup.filter = filterValue.trim().toLowerCase();
  }
}
