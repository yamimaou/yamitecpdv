import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  products : any;
  columnsNames: string[] = [ 'NOME', 'DESCRIÇÃO', 'PREÇO', 'AÇÃO'];
  dataColumns: string[] = ['product_name', 'product_description', 'product_price', 'acao'];
  isLoadingResults = true;
  constructor(private _api: ApiService) { }
  ngOnInit() {
    this._api.getProdutos().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.products = response.data;
      this.isLoadingResults = false;
    }, err => {
      console.error(err);
       this.isLoadingResults = false;
    });
  }
  applyFilter(filterValue: string, formGroup : any) {
    formGroup.filter = filterValue.trim().toLowerCase();
  }
}
