import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { Produto } from 'src/model/produto';
import { DeleteDialogComponent } from 'src/app/components/dialogs/delete-dialog/delete-dialog.component';
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  title : 'deseja ?';
  produto: any;
  columnsNames: string[] = [ 'PREÇO', 'FORNECEDOR', 'PRODUTO DE AMOSTRA','PESO', 'TAMANHO','ATUALIZADO EM','CADASTRO'];
  dataColumns: string[] = [ 'product_price', 'product_provider', 'product_sample', 'product_weight', 'product_scale','updated_at','created_at'];
  dataVer = {
    product_price : " | currency:'BRL':true",
    product_sample : " == 0 ? 'NÃO' : 'SIM'",
  }

  loteColumns: string[] = ['productlote_code','user_name', 'productlote_qtd', 'productlote_price', 'productlote_waranty','productlote_validity','created_at'];
  loteNames: string[] = ['CODIGO','USUÁRIO', 'QTD', 'PREÇO', 'GARANTIA','VALIDADE','CADASTRO'];
  lotes = [];
  movtaColumns: string[] = ['client_id', 'productmovta_client_id', 'user_id', 'created_at','productmovta_qtd','productmovta_obs'];
  movtaNames: string[] = ['DE', 'PARA', 'POR', 'EM','QUANTIDADE','OBS'];
  movta = [];
  clientColumns: string[] = ['client_companyname', 'client_fantasyname', 'client_cnpj', 'client_ie','created_at'];
  clientNames: string[] = ['RAZÃO SOCIAL', 'NOME FANTASIA', 'CNPJ', 'IE','CADASTRO'];
  clients = [];
  isLoadingResults = true;
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private deleteDialog : DeleteDialogComponent) { }


  ngOnInit() {
    this.getProduto(this.route.snapshot.params['id']);
  }
  getDialog(id){
    this.deleteDialog.title = 'Excluír Registro';
    this.deleteDialog.message = 'Deseja Realmente Excluír o registro?'
    this.deleteDialog.openDialog(() => {
      this.deleteProduto(id);
      this.router.navigate(['/produtos']);
    });

  }
  getProduto(id) {
    this.api.getProduto(id)
      .subscribe(data => {
        let response = JSON.parse(JSON.stringify(data));
        if(response.length == 0){
          this.isLoadingResults = false;
          this.produto = response;
          console.log(this.produto);
        return this.produto;
        }
        this.produto = response.data;
        this.lotes = response.data.lotes;
        this.movta = response.data.movta;
        this.clients = response.data.clients;
        console.log(this.produto);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  deleteProduto(id) {
    this.isLoadingResults = true;
    this.api.deleteProduto(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          //this.router.navigate(['produtos']);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }
}