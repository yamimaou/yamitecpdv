import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators, ValidationErrors } from '@angular/forms';
import { ApiService } from '../../../services/api.service';
import { Produto } from 'src/model/produto';
import { Category } from 'src/model/category';
import { MatSnackBar, MatSnackBarRef, MatTableDataSource } from '@angular/material';
import { Lote } from 'src/model/lote';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  products: Produto;
  categories: Category;
  lotes = new MatTableDataSource<Element[]>();
  lotesColumns: string[] = ['code', 'price', 'qtd', 'waranty', 'validity'];
  productForm: FormGroup;
  loteForm: FormGroup;
  isLoadingResults = false;

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      //'product_id': [1],
      'product_name': ['', [Validators.required, Validators.minLength(4)]],
      'product_provider': ['', [Validators.required, Validators.minLength(2)]],
      'product_description': ['', [Validators.required, Validators.minLength(10)]],
      'product_price': ['0.00', Validators.required],
      'product_code': [''],
      'product_sample': ['0'],
      'product_waranty': [null],
      'product_weight': ['0.00'],
      'product_scale': ['0,0,0'],
      'category_id': [1],
    });
    this.loteForm = this.formBuilder.group({
      'product_id': [1],
      'productlote_code': [null, [Validators.required, Validators.minLength(4)]],
      'productlote_price': [null, [Validators.required, Validators.minLength(2)]],
      'productlote_qtd': [1, Validators.required],
      'productlote_waranty': [Date.now],
      'productlote_validity': [Date.now, Validators.required]
    });
    this.api.getCategories().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.categories = response.data;
      console.log(this.categories);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
  addProduto(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addProduto(form)
      .subscribe(res => {
        let response = JSON.parse(JSON.stringify(res));
        console.log(response.lastId);
        this.loteForm.controls['product_id'].setValue(response.lastId);
        this.isLoadingResults = false;
        this.productForm.reset();
        this.snackBar.open("Produto Adicionado.",null, {
          duration : 2000,
        });
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao adicionar produto.",null, {
          duration : 2000,
        });
        this.isLoadingResults = false;
      });
  }
  addLote(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addLote(form)
      .subscribe(res => {
        let response = JSON.parse(JSON.stringify(res));
        console.log(response.lastId);
        let nwLote = this.lotes.data;
        nwLote.push(this.loteForm.value);
        this.lotes.data = nwLote;
        this.isLoadingResults = false;
        this.loteForm.controls['productlote_code'].setValue(null);
        this.loteForm.controls['productlote_price'].setValue(null);
        this.loteForm.controls['productlote_qtd'].setValue(1);
        this.snackBar.open("Lote Adicionado ao produto.",null, {
          duration : 2000,
        });
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao adicionar Lote.",null, {
          duration : 2000,
        });
        this.isLoadingResults = false;
      });
  }

  getFormValidationErrors() {
    Object.keys(this.productForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.productForm.get(key).errors;
      const translateErrors = {
        'required': ' campo Obrigatório',
        'minLength': ' minimo de caracteres não alcançado',
        'maxLength': ' máximo de caracteres atingido/ultrapassado'
      }

      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          this.snackBar.open('O Campo \"' + key + "\" apresentou o seguinte problema : " + translateErrors[keyError], null, {
            duration: 5000,
          });
        });
      }
    });
  }
  applyFilter(filterValue: string, formGroup: any) {
    formGroup.filter = filterValue.trim().toLowerCase();
  }
}
