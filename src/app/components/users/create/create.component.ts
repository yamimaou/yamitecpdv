import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, ValidationErrors } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  user: any;
  clients: any;
   _id : number;
  userForm: FormGroup;
  isLoadingResults = true;
  dataformCampos = [];

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder, public snackBar: MatSnackBar) { 
   
  }

  ngOnInit() {
    this._id = 1;
    this.getUser(this._id);    
    this.api.getClients().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.clients = response.data.map((v) => {
        return { name: v.client_fantasyname, value: v.client_id } 
      });
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
  testparam(param){
    console.log(param);
  }
  getUser(id) {
    this.api.getUser(id)
      .subscribe(data => {
        let response = JSON.parse(JSON.stringify(data))
        this.user = response.data;
        if(response.length == 0){
          this.user = response;
        return this.user;
        }
        this.dataformCampos = [
          {
            colname: 'client_id',
            coltype: 'select',
            collabel: 'Cliente',
            colvalue: null,
            colattr : null,
            coldata: this.clients
          },
          {
            colname: 'user_name',
            coltype: 'text',
            collabel: 'NOME',
            colvalue: null,
            colvalidators: [Validators.required, Validators.minLength(4)],
            colattr : null,
            coldata: [
              { name: 'teste', value: 5 },
              { name: 'teste 1', value: 2 },
              { name: 'teste 2', value: 3 },
            ]
          },
          {
            colname: 'user_email',
            coltype: 'text',
            collabel: 'EMAIL',
            colvalue: null,
            colvalidators: [Validators.required, Validators.minLength(4)],
            colattr : null,
            coldata: [
              { name: 'teste', value: 5 },
              { name: 'teste 1', value: 2 },
              { name: 'teste 2', value: 3 },
            ]
          },
          {
            colname: 'user_status',
            coltype: 'select',
            collabel: 'STATUS',
            colvalue: null,
            colvalidators: [Validators.required],
            colattr : null,
            coldata: [
              { name: 'ATIVO', value: 1 },
              { name: 'DESATIVADO', value: 2 },
              { name: 'INATIVO', value: 0 },
            ]
          },
          {
            colname: 'user_profile',
            coltype: 'select',
            collabel: 'PERFIL',
            colvalue: null,
            colvalidators: [Validators.required],
            colattr : null,
            coldata: [
              { name: 'GERENTE', value: 0 },
              { name: 'TECNICO', value: 1 },
              { name: 'VENDEDOR', value: 2 },
            ]
          },
          {
            colname: 'user_type',
            coltype: 'radio',
            collabel: 'TIPO',
            colvalue: null,
            colvalidators: Validators.required,
            colattr : null,
            coldata: [
              { name: 'COMUM', value: 1 },
              { name: 'ADMINISTRADOR', value: 0 },
            ]
          },
        ]
        console.log(this.user);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }
  addUser(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addUser(form)
      .subscribe(res => {
        let response = JSON.parse(JSON.stringify(res));
        console.log(response.lastId);
        this.isLoadingResults = false;
        this.snackBar.open("Usuário Adicionado.",null, {
          duration : 2000,
        });
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao adicionar Usuário.",null, {
          duration : 2000,
        });
        this.isLoadingResults = false;
      });
  }
  EditUser(form: NgForm) {
    this.isLoadingResults = true;
    this.api.updateUser(this._id,form)
      .subscribe(res => {
        this.isLoadingResults = false;
        this.snackBar.open("Usuário Atualizado com sucesso.",null, {
          duration : 2000,
        });
        this.router.navigate(['/usuarios/'+this._id]);
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao atualizar usuario.",null, {
          duration : 2000,
        });
        this.isLoadingResults = false;
      });
  }
  getFormValidationErrors() {
    Object.keys(this.userForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.userForm.get(key).errors;
      const translateErrors = {
        'required': ' campo Obrigatório',
        'minLength': ' minimo de caracteres não alcançado',
        'maxLength': ' máximo de caracteres atingido/ultrapassado'
      }

      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          this.snackBar.open('O Campo \"' + key + "\" apresentou o seguinte problema : " + translateErrors[keyError], null, {
            duration: 5000,
          });
        });
      }
    });
  }

}
