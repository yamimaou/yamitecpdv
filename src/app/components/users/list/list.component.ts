import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  users : any;
  columnsNames: string[] = [ 'NOME', 'EMAIL', 'CADASTRO','AÇÕES'];
  dataColumns: string[] = [ 'user_name', 'user_email', 'created_at','acao'];
  isLoadingResults = true;
  constructor(private _api: ApiService) { }
  ngOnInit() {
    this._api.getUsers().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.users = response.data;
      this.isLoadingResults = false;
    }, err => {
      console.error(err);
       this.isLoadingResults = false;
    });
  }

}
