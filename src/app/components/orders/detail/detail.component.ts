import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { DeleteDialogComponent } from '../../dialogs/delete-dialog/delete-dialog.component';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  title : 'deseja ?';
  orders: any;
  columnsNames: string[] = [ 'CLIENTE', 'TITULO', 'DESCRIÇÃO','PRIORIDADE','STATUS'];
  dataColumns: string[] = [ 'user_id', 'order_title', 'order_description','order_priority','order_status'];
  clientNames: string[] = [ 'RAZÃO SOCIAL', 'NOME FANTASIA', 'CNPJ','IE', 'CADASTRO', 'AÇÃO'];
  clientColumns: string[] = [ 'client_companyname', 'client_fantasyname', 'client_cnpj', 'client_ie', 'created_at','acao'];
  
  dataVer = {}
  isLoadingResults = true;
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private deleteDialog : DeleteDialogComponent,public snackBar: MatSnackBar) { }


  ngOnInit() {
    this.getOrder(this.route.snapshot.params['id']);
  }

  getDialog(id){
    this.deleteDialog.title = 'Excluír Registro';
    this.deleteDialog.message = 'Deseja Realmente Excluír o registro?'
    this.deleteDialog.openDialog(() => {
      this.deleteOrder(id);
      this.router.navigate(['/usuarios']);
    });
  }

  getOrder(id) {
    this.api.getOrder(id)
      .subscribe(data => {
        let response = JSON.parse(JSON.stringify(data));
        if(response.length == 0){
          this.isLoadingResults = false;
          this.orders = response;
        return this.orders;
        }
        this.orders = response.data;
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  deleteOrder(id) {
    this.isLoadingResults = true;
    this.api.deleteOrder(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.snackBar.open("O.S Excluído com sucesso.",null, {
            duration : 2000,
          });
          this.router.navigate(['usuarios']);
        }, (err) => {
          console.log(err);
          this.snackBar.open(err,null, {
            duration : 2000,
          });
          this.isLoadingResults = false;
        }
      );
  }

}
