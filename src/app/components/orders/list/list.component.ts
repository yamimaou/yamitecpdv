import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  orders : any;
  actual_userData = JSON.parse(localStorage.getItem('userData'));
  orderServiceUrl = this.actual_userData.data.user_profile == 1 ? 'ordens' : 'ordens/working'
  columnsNames: string[] = [ 'CLIENTE', 'TITULO', 'DESCRIÇÃO','PRIORIDADE','STATUS','ATENDIMENTO','AÇÕES'];
  dataColumns: string[] = [ 'user_id', 'order_title', 'order_description','order_priority','order_status','user_created_id','acao'];
  isLoadingResults = true;
  constructor(private _api: ApiService) { }
  ngOnInit() {
    this._api.getOrders().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.orders = response
      .data;
      this.isLoadingResults = false;
    }, err => {
      console.error(err);
       this.isLoadingResults = false;
    });
  }

}
