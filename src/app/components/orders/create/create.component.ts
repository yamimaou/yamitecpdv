import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, ValidationErrors } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  order: any;
  users: any;
  priority: any;
  status: any;
  _id: number;
  orderForm: FormGroup;
  isLoadingResults = true;
  dataformCampos = [];

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder, public snackBar: MatSnackBar) {

  }
  ngOnInit() {
    this.setDataforms();
    this.getUsers();
    this.getPriority();
    this.getStatus();
  }
  getUsers() {
    this.api.getUsers().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      
      this.users = response.length == 0 ? [{name: "Nada encontrado", value : null}] :response.data.map((v) => {
        return { name: v.user_name, value: v.user_id }
      });
      this.setDataforms()
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
  getPriority() {
    this.api.getPriority().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.priority = response.length == 0 ? [{name: "Nada encontrado", value : null}] :response.data.map((v) => {
        return { name: v.priority_name, value: v.priority_id }
      });
      this.setDataforms()
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
  getStatus() {
    this.api.getStatus().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.status = response.length == 0 ? [{name: "Nada encontrado", value : null}] :response.data.map((v) => {
        return { name: v.status_name, value: v.status_id }
      });
      this.setDataforms()
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
  testparam(param) {
    console.log(param);
  }
  addOrder(form: NgForm) {
    this.isLoadingResults = true;
    let ret = JSON.parse(JSON.stringify(form))
    this.api.getUser(ret.user_id).subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      ret.client_id = response.data.client_id;
      /**** send data */
      this.api.addOrder(ret)
      .subscribe(res => {
        let response = JSON.parse(JSON.stringify(res));
        console.log(response.lastId);
        this.isLoadingResults = false;
        this.snackBar.open("Ordem de Serviço Adicionada.", null, {
          duration: 2000,
        });
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao adicionar Ordem de Serviço.", null, {
          duration: 2000,
        });
        this.isLoadingResults = false;
      });

    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
  EditUser(form: NgForm) {
    this.isLoadingResults = true;
    this.api.updateUser(this._id, form)
      .subscribe(res => {
        this.isLoadingResults = false;
        this.snackBar.open("Usuário Atualizado com sucesso.", null, {
          duration: 2000,
        });
        this.router.navigate(['/usuarios/' + this._id]);
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao atualizar usuario.", null, {
          duration: 2000,
        });
        this.isLoadingResults = false;
      });
  }
  setDataforms() {
    this.dataformCampos = [
      {
        colname: 'user_id',
        coltype: 'select',
        collabel: 'CLIENTE',
        colvalue: null,
        colattr: null,
        coldata: this.users
      },
      {
        colname: 'order_title',
        coltype: 'text',
        collabel: 'TITULO',
        colvalue: null,
        colvalidators: [Validators.required, Validators.minLength(4)],
        colattr: null,
        coldata: null
      },
      {
        colname: 'order_description',
        coltype: 'textarea',
        collabel: 'DESCRIÇÃO',
        colvalue: null,
        colvalidators: [Validators.required, Validators.minLength(10)],
        colattr: null,
        coldata: null
      },
      {
        colname: 'order_priority',
        coltype: 'select',
        collabel: 'PRIORIDADE',
        colvalue: 3,
        colvalidators: [Validators.required],
        colattr: null,
        coldata: this.priority
      },
      {
        colname: 'order_status',
        coltype: 'select',
        collabel: 'STATUS',
        colvalue: 1,
        colvalidators: [Validators.required],
        colattr: null,
        coldata: this.status
      },
    ]
  }
  getFormValidationErrors() {
    Object.keys(this.orderForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.orderForm.get(key).errors;
      const translateErrors = {
        'required': ' campo Obrigatório',
        'minLength': ' minimo de caracteres não alcançado',
        'maxLength': ' máximo de caracteres atingido/ultrapassado'
      }

      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          this.snackBar.open('O Campo \"' + key + "\" apresentou o seguinte problema : " + translateErrors[keyError], null, {
            duration: 5000,
          });
        });
      }
    });
  }
}
