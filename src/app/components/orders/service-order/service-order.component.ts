import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-service-order',
  templateUrl: './service-order.component.html',
  styleUrls: ['./service-order.component.css']
})
export class ServiceOrderComponent implements OnInit {
  constructor(private _api: ApiService, private route: ActivatedRoute, private snackbar: MatSnackBar, private formBuilder: FormBuilder) { }
  order_title = new FormControl('');
  order_description = new FormControl('');
  orders: any;
  products: any;
  services: any;
  productSelected: any;
  serviceSelected: any;
  ngOnInit() {

    this._api.getOrder(this.route.snapshot.params['id']).subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.orders = response.length == 0 ? [] : response.data
      this.productSelected = response.data.products.length == 0 ? [] : response.data.products.map((v) => {
        return { name: v.product_name, value: v.product_id }
      });
      this.serviceSelected = response.data.services.length == 0 ? [] : response.data.services.map((v) => {
        return { name: v.service_name, value: v.service_id }
      });
    }, err => {
      console.log(err);
    });
    this.getData();
  }
  getData() {
    this._api.getProdutos().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      let arrn = [];
      this.productSelected != undefined ? this.productSelected.forEach(element => {
        arrn.push(element.value);
      }) : console.log(this.productSelected);
      this.products = response.length == 0 ? [{ name: "Nada encontrado", value: null }] : response.data
        .filter((data) => !arrn.includes(data.product_id))
        .map((v) => {
          return { name: v.product_name, value: v.product_id }
        });
      console.log("PROD");
      console.log(this.products);
    }, err => {
      console.log(err);
    });

    this._api.getServicos().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      let arrs = [];
      this.serviceSelected != undefined ? this.serviceSelected.forEach(element => {
        console.log(element)
        arrs.push(element.value);
      }) : console.log(this.serviceSelected);
      console.log(arrs);
      this.services = response.length == 0 ? [{ name: "Nada encontrado", value: null }] : response.data
        .filter((data) => !arrs.includes(data.service_id))
        .map((v) => {
          return { name: v.service_name, value: v.service_id }
        });
    }, err => {
      console.log(err);
    });
  }
  updateData() {
    let osProducts = []
    let osServices = []
    let profileData = JSON.parse(localStorage.getItem('userData')).data

    this.productSelected.map((v, k) => {
      osProducts.push({
        order_id: this.route.snapshot.params['id'],
        user_id: profileData.user_id,
        product_id: v.value
      });
    });
    console.log(osProducts);
    this.serviceSelected.map((v, k) => {
      osServices.push({
        order_id: this.route.snapshot.params['id'],
        user_id: profileData.user_id,
        service_id: v.value
      });
    });
    console.log(osServices)
    this._api.addOrderItens(this.route.snapshot.params['id'], osProducts).subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      console.log(response.lastId);
      this.snackbar.open("Produtos Adicionados com êxito.", null, {
        duration: 2000,
      });
    }, (err) => {
      console.log(err);
      this.snackbar.open("Problema ao adicionar Ordem de Serviço.", null, {
        duration: 2000,
      });
    });
    this._api.addOrderItens(this.route.snapshot.params['id'], osServices).subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      console.log(response.lastId);
      this.snackbar.open("Serviços Adicionados.", null, {
        duration: 2000,
      });
    }, (err) => {
      console.log(err);
      this.snackbar.open("Problema ao adicionar Serviços.", null, {
        duration: 2000,
      });
    });
    let osupdt = {
      order_status: 7,
      order_description: this.orders.order_description + "\rTÉCNICO : \r" + this.order_description.value
    }
    console.log(osupdt);
    this._api.updateOrder(this.route.snapshot.params['id'], osupdt).subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      console.log(response.lastId);
      this.snackbar.open("OS Atualizada com êxito", null, {
        duration: 2000,
      });
    }, (err) => {
      console.log(err);
      this.snackbar.open("Problema ao atualizar Ordem de Serviço.", null, {
        duration: 2000,
      });
    });

  }
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

}
