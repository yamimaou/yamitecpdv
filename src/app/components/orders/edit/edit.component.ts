import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, ValidationErrors } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-create',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  order: any;
   _id : number;
   priority: [];
   status: [];
  orderForm: FormGroup;
  isLoadingResults = true;
  dataformCampos = [];

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder, public snackBar: MatSnackBar) { 
   
  }

  ngOnInit() {
    this._id = this.route.snapshot.params['id'];
    this.getOrder(this._id);    
  }
  testparam(param){
    console.log(param);
  }
  getOrder(id) {
    this.api.getOrder(id)
      .subscribe(data => {
        let response = JSON.parse(JSON.stringify(data))
        this.order = response.data;
        if(response.length == 0){
          this.order = response;
        return this.order;
        }
        this.dataformCampos = [
          {
            colname: 'order_id',
            coltype: 'hidden',
            collabel: 'id',
            colvalue: this.order.order_id,
            colattr : null,
            coldata: []
          },
          {
            colname: 'order_title',
            coltype: 'text',
            collabel: 'TITULO',
            colvalue: this.order.order_title,
            colvalidators: [Validators.required, Validators.minLength(4)],
            colattr : null,
            coldata: null
          },
          {
            colname: 'order_description',
            coltype: 'text',
            collabel: 'DESCRIÇÃO',
            colvalue: this.order.order_description,
            colvalidators: [Validators.required, Validators.minLength(4)],
            colattr : null,
            coldata: null
          },
          {
            colname: 'order_status',
            coltype: 'select',
            collabel: 'STATUS',
            colvalue: this.order.order_status,
            colvalidators: [Validators.required],
            colattr : null,
            coldata: [
              { name: 'ATIVO', value: 1 },
              { name: 'DESATIVADO', value: 2 },
              { name: 'INATIVO', value: 0 },
            ]
          },
          {
            colname: 'order_priority',
            coltype: 'select',
            collabel: 'PRIORIDADE',
            colvalue: this.order.order_priority,
            colvalidators: [Validators.required],
            colattr : null,
            coldata: [
              { name: 'GERENTE', value: 0 },
              { name: 'TECNICO', value: 1 },
              { name: 'VENDEDOR', value: 2 },
            ]
          },
        ]
        console.log(this.order);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  EditOrder(form: NgForm) {
    this.isLoadingResults = true;
    this.api.updateOrder(this._id,form)
      .subscribe(res => {
        this.isLoadingResults = false;
        this.snackBar.open("Ordem Atualizado com sucesso.",null, {
          duration : 2000,
        });
        this.router.navigate(['/ordens/'+this._id]);
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao atualizar Ordem.",null, {
          duration : 2000,
        });
        this.isLoadingResults = false;
      });
  }
  getFormValidationErrors() {
    Object.keys(this.orderForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.orderForm.get(key).errors;
      const translateErrors = {
        'required': ' campo Obrigatório',
        'minLength': ' minimo de caracteres não alcançado',
        'maxLength': ' máximo de caracteres atingido/ultrapassado'
      }

      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          this.snackBar.open('O Campo \"' + key + "\" apresentou o seguinte problema : " + translateErrors[keyError], null, {
            duration: 5000,
          });
        });
      }
    });
  }

  setDataforms(){
    this.dataformCampos = [
      {
        colname: 'order_id',
        coltype: 'hidden',
        collabel: 'id',
        colvalue: this.order.order_id,
        colattr : null,
        coldata: []
      },
      {
        colname: 'order_title',
        coltype: 'text',
        collabel: 'TITULO',
        colvalue: this.order.order_title,
        colvalidators: [Validators.required, Validators.minLength(4)],
        colattr : null,
        coldata: null
      },
      {
        colname: 'order_description',
        coltype: 'text',
        collabel: 'DESCRIÇÃO',
        colvalue: this.order.order_description,
        colvalidators: [Validators.required, Validators.minLength(4)],
        colattr : null,
        coldata: null
      },
      {
        colname: 'order_status',
        coltype: 'select',
        collabel: 'STATUS',
        colvalue: this.order.order_status,
        colvalidators: [Validators.required],
        colattr : null,
        coldata: [
          { name: 'ATIVO', value: 1 },
          { name: 'DESATIVADO', value: 2 },
          { name: 'INATIVO', value: 0 },
        ]
      },
      {
        colname: 'order_priority',
        coltype: 'select',
        collabel: 'PRIORIDADE',
        colvalue: this.order.order_priority,
        colvalidators: [Validators.required],
        colattr : null,
        coldata: [
          { name: 'GERENTE', value: 0 },
          { name: 'TECNICO', value: 1 },
          { name: 'VENDEDOR', value: 2 },
        ]
      },
    ]
  }

  getPriority() {
    this.api.getPriority().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.priority = response.length == 0 ? [{name: "Nada encontrado", value : null}] :response.data.map((v) => {
        return { name: v.priority_name, value: v.priority_id }
      });
      this.setDataforms();
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
  getStatus() {
    this.api.getStatus().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.status = response.length == 0 ? [{name: "Nada encontrado", value : null}] :response.data.map((v) => {
        return { name: v.status_name, value: v.status_id }
      });
      this.setDataforms()
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
}
