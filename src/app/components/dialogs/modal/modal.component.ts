import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @Input() title : string;
  @Input() confirm : string;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    //this.openDialog(null)
  }
  
  openDialog(method: any = ()=>{ }) {
    const dialogRef = this.dialog.open(ModalComponent);
    dialogRef.afterClosed().subscribe( result => {
      if (result) {
        if(method !== null) method();
      }
    });
  }
}