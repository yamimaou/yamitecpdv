import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css']
})
export class DeleteDialogComponent implements OnInit {
  public title : string;
  public message : string = "M";

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    
  }
  
  openDialog(method: any = ()=>{ }) {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe( result => {
      if (result) {
        method();
      }
    });
  }
}

@Component({
  selector: 'dialog-content-delete',
  templateUrl: 'dialog-content-delete.html',
})
export class DialogContentDelete { }
