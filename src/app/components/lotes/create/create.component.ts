import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder, NgForm } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { MatSnackBar, MatTableDataSource } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Produto } from 'src/model/produto';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  loteForm: FormGroup;
  loteColumns: string[] = ['lcode','lusername', 'lqtd', 'lprice', 'lwarranty','lvalidity','lcreated_at'];
  isLoadingResults = false;
  produto: Produto = { 
    category_id : 0,
    user_id: 0,
    client_id : 0,
    product_id: 0,
    product_name: '',
    product_provider: '',
    product_description: '',
    product_price: 0,
    product_sample: 0,
    product_code: '',
    product_waranty: '',
    product_weight: 0,
    product_scale: '',
    created_at: null,
    updated_at: null,
    users: [],
    lotes: [],
    movta: [],
    clients: [],
  };
  lotes = new MatTableDataSource<Element[]>();
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder, public snackBar: MatSnackBar) { }

  ngOnInit() {
    const _id = this.route.snapshot.params['id'];
    this.getProduto(_id);
    this.loteForm = this.formBuilder.group({
      'product_id': [_id],
      'productlote_code': [null, [Validators.required, Validators.minLength(4)]],
      'productlote_price': [null, [Validators.required, Validators.minLength(2)]],
      'productlote_qtd': [1, Validators.required],
      'productlote_waranty': [null],
      'productlote_validity': [null, Validators.required]
    });
  }
  getProduto(id) {
    this.api.getProduto(id)
      .subscribe(data => {
        let response = JSON.parse(JSON.stringify(data))
        this.produto = response.data;
        console.log(this.produto);
        this.isLoadingResults = false;
      });
  }
  addLote(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addLote(form)
      .subscribe(res => {
        let response = JSON.parse(JSON.stringify(res));
        console.log(response.lastId);
        let nwLote = this.lotes.data;
        nwLote.push(this.loteForm.value);
        this.lotes.data = nwLote;
        this.isLoadingResults = false;
        this.loteForm.controls['productlote_code'].setValue(null);
        this.loteForm.controls['productlote_price'].setValue(null);
        this.loteForm.controls['productlote_qtd'].setValue(1);
        this.loteForm.reset();
        this.loteForm.updateValueAndValidity();
      }, (err) => {
        console.log(err);
        this.snackBar.open(err);
        this.isLoadingResults = false;
      });
  }

}
