import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: any;
  isLoadingResults: boolean;
  usrForm : any
  constructor(private _api: ApiService) { }

  ngOnInit() {
    localStorage.getItem('userData') ? window.location.href = '/ordens' : console.log('efetuar login');
    this.setDataForm();
  }
  login(usrData){
     this._api.getToken(usrData)
    .subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.user = response;
      localStorage.setItem('userData', JSON.stringify(this.user));
      console.log(JSON.parse(localStorage.getItem('userData')))
      this.isLoadingResults = false;
      window.location.href = '/ordens';
    }, err => {
      console.error(err);
       this.isLoadingResults = false;
    });
  }
  setDataForm(){
    this.usrForm = [
      {
        colname: 'user',
        coltype: 'text',
        collabel: 'Usuário',
        colvalue: null,
        colvalidators: [Validators.required, Validators.minLength(2)],
        colattr: null,
        coldata: null
      },
      {
        colname: 'pwd',
        coltype: 'password',
        collabel: 'Senha',
        colvalue: null,
        colvalidators: [Validators.required, Validators.minLength(2)],
        colattr: null,
        coldata: null
      },
    ]
  }

}
