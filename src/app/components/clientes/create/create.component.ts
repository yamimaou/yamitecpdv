import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators, ValidationErrors } from '@angular/forms';
import { ApiService } from '../../../services/api.service';
import { MatSnackBar, MatSnackBarRef, MatTableDataSource } from '@angular/material';
import { Organizacao } from 'src/model/organizacao';
@Component({
  selector: 'app-create-client',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  orgs: Organizacao;
  addresses : []
  clientsForm: FormGroup;
  addressForm: FormGroup;
  unitiesForm: FormGroup;
  taxesForm: FormGroup;
  clientesColumns: string[] = ['companyname', 'fantasyname', 'cnpj', 'ie', 'created', 'acao'];
  clientes = new MatTableDataSource<Element[]>();
  isLoadingResults = false;
  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder, public snackBar: MatSnackBar) { }
  ngOnInit() {
    this.getOrgs();
    this.clientsForm = this.formBuilder.group({
      'org_id': [1],
      'client_companyname': [null, [Validators.required, Validators.minLength(4)]],
      'client_fantasyname': [null, [Validators.required, Validators.minLength(2)]],
      'client_cnpj': [null, [Validators.required, Validators.minLength(10)]],
      'client_ie': [null],
    });
    this.addressForm = this.formBuilder.group({
      'client_id': [1],
      'address_street': [null, [Validators.required, Validators.minLength(2)]],
      'address_neighborhood': [null, [Validators.required, Validators.minLength(2)]],
      'address_cep': [null, [Validators.required, Validators.minLength(3)]],
      'address_city': [null, [Validators.required, Validators.minLength(2)]],
      'address_state': [null, [Validators.required, Validators.minLength(2)]],
      'address_country': [null, [Validators.required, Validators.minLength(2)]],
      'address_number': [null, [Validators.required, Validators.minLength(1)]],
    });
    this.unitiesForm = this.formBuilder.group({
      'client_id': [1],
      'address_id': [null, [Validators.required, Validators.minLength(1)]],
      'unity_name': [null, [Validators.required, Validators.minLength(2)]],
    });
    this.taxesForm = this.formBuilder.group({
      'client_id': [1],
      'taxes_icms': [null, [Validators.required, Validators.minLength(1)]],
      'taxes_iis': [null, [Validators.required, Validators.minLength(1)]],
      'taxes_irpj': [null, [Validators.required, Validators.minLength(1)]],
      'taxes_csll': [null, [Validators.required, Validators.minLength(1)]],
      'taxes_cofins': [null, [Validators.required, Validators.minLength(1)]],
      'taxes_pis': [null, [Validators.required, Validators.minLength(1)]],
      'taxes_discont': [null, [Validators.required, Validators.minLength(1)]],
    });
  }
  getOrgs() {
    this.api.getOrgs().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.orgs = response.data;
    });
  }
  addClient(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addCliente(form)
      .subscribe(res => {
        let response = JSON.parse(JSON.stringify(res));
        console.log(response.lastId);
        this.unitiesForm.controls['client_id'].setValue(response.lastId);
        this.addressForm.controls['client_id'].setValue(response.lastId);
        this.taxesForm.controls['client_id'].setValue(response.lastId);
        this.isLoadingResults = false;
        this.clientsForm.reset();
        this.snackBar.open("Organização Adicionada.", null, {
          duration: 2000,
        });
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao adicionar Organização.", null, {
          duration: 2000,
        });
        this.isLoadingResults = false;
      });
  }
  addAddress(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addEndereco(form)
      .subscribe(res => {
        let response = JSON.parse(JSON.stringify(res));
        console.log(response.lastId);
        this.unitiesForm.controls['address_id'].setValue(response.lastId);
        this.isLoadingResults = false;
        this.clientsForm.reset();
        this.snackBar.open("Endereço Adicionado.", null, {
          duration: 2000,
        });
        this.api.getEnderecos().subscribe(res => {
          let response = JSON.parse(JSON.stringify(res));
          this.addresses = response.data;
        })
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao adicionar Endereço.", null, {
          duration: 2000,
        });
        this.isLoadingResults = false;
      });
  }
  addUnity(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addUnidade(form)
      .subscribe(res => {
        let response = JSON.parse(JSON.stringify(res));
        console.log(response.lastId);
        this.isLoadingResults = false;
        this.clientsForm.reset();
        this.snackBar.open("Unidade Adicionado.", null, {
          duration: 2000,
        });
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao adicionar Unidade.", null, {
          duration: 2000,
        });
        this.isLoadingResults = false;
      });
  }
  addTaxes(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addTaxas(form)
      .subscribe(res => {
        let response = JSON.parse(JSON.stringify(res));
        console.log(response.lastId);
        this.isLoadingResults = false;
        this.clientsForm.reset();
        this.snackBar.open("Impostos Adicionados.", null, {
          duration: 2000,
        });
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao adicionar Impostos.", null, {
          duration: 2000,
        });
        this.isLoadingResults = false;
      });
  }
  getFormValidationErrors() {
    Object.keys(this.clientsForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.clientsForm.get(key).errors;
      const translateErrors = {
        'required': ' campo Obrigatório',
        'minLength': ' minimo de caracteres não alcançado',
        'maxLength': ' máximo de caracteres atingido/ultrapassado'
      }

      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          this.snackBar.open('O Campo \"' + key + "\" apresentou o seguinte problema : " + translateErrors[keyError], null, {
            duration: 5000,
          });
        });
      }
    });
  }
  applyFilter(filterValue: string, formGroup: any) {
    formGroup.filter = filterValue.trim().toLowerCase();
  }
}
