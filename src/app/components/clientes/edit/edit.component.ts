import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators, ValidationErrors } from '@angular/forms';
import { ApiService } from '../../../services/api.service';
import { Clientes } from 'src/model/Clientes';
import { Category } from 'src/model/category';
import { MatSnackBar, MatSnackBarRef, MatTableDataSource } from '@angular/material';
import { Lote } from 'src/model/lote';

@Component({
  selector: 'app-create',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  Cliente: Clientes;
   _id : number;
   ClienteForm: FormGroup;
  isLoadingResults = true;

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder, public snackBar: MatSnackBar) { 
   
  }

  ngOnInit() {
    this._id = this.route.snapshot.params['id'];
    this.getCliente(this._id);
  }
  getCliente(id) {
    this.api.getClient(id)
      .subscribe(data => {
        let response = JSON.parse(JSON.stringify(data))
        this.Cliente = response.data;
        if(response.length == 0){
          this.isLoadingResults = false;
          this.Cliente = response;
          console.log(this.Cliente);
        return this.Cliente;
        }
        this.ClienteForm = this.formBuilder.group({
          'client_companyname': [this.Cliente.client_companyname, [Validators.required, Validators.minLength(2)]],
          'client_fantasyname': [this.Cliente.client_fantasyname, [Validators.required, Validators.minLength(2)]],
          'client_cnpj': [this.Cliente.client_cnpj, [Validators.required, Validators.minLength(10)]],
          'client_ie': [this.Cliente.client_ie],
        });
        console.log(this.Cliente);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  EditCliente(form: NgForm) {
    this.isLoadingResults = true;
    this.api.updateCliente(this._id,form)
      .subscribe(res => {
        this.isLoadingResults = false;
        this.ClienteForm.reset();
        this.snackBar.open("Cliente Atualizado com sucesso.",null, {
          duration : 2000,
        });
        this.router.navigate(['/clientes/'+this._id]);
      }, (err) => {
        console.log(err);
        this.snackBar.open("Problema ao atualizar Cliente.",null, {
          duration : 2000,
        });
        this.isLoadingResults = false;
      });
  }
  getFormValidationErrors() {
    Object.keys(this.ClienteForm.controls).forEach(key => {
      const controlErrors: ValidationErrors = this.ClienteForm.get(key).errors;
      const translateErrors = {
        'required': ' campo Obrigatório',
        'minLength': ' minimo de caracteres não alcançado',
        'maxLength': ' máximo de caracteres atingido/ultrapassado'
      }

      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          this.snackBar.open('O Campo \"' + key + "\" apresentou o seguinte problema : " + translateErrors[keyError], null, {
            duration: 5000,
          });
        });
      }
    });
  }
  applyFilter(filterValue: string, formGroup: any) {
    formGroup.filter = filterValue.trim().toLowerCase();
  }
}
