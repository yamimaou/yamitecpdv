import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { Clientes } from 'src/model/clientes';
import { DeleteDialogComponent } from 'src/app/components/dialogs/delete-dialog/delete-dialog.component';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  title : 'deseja ?';
  clientes: any;
  columnsNames: string[] = [ 'RAZÃO SOCIAL', 'NOME FANTASIA', 'CNPJ','IE', 'CADASTRO'];
  dataColumns: string[] = [ 'client_companyname', 'client_fantasyname', 'client_cnpj', 'client_ie', 'created_at'];
  dataVer = {}
  isLoadingResults = true;
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private deleteDialog : DeleteDialogComponent,public snackBar: MatSnackBar) { }


  ngOnInit() {
    this.getCliente(this.route.snapshot.params['id']);
  }

  getDialog(id){
    this.deleteDialog.title = 'Excluír Registro';
    this.deleteDialog.message = 'Deseja Realmente Excluír o registro?'
    this.deleteDialog.openDialog(() => {
      this.deleteCliente(id);
      this.router.navigate(['/clientes']);
    });
  }

  getCliente(id) {
    this.api.getClient(id)
      .subscribe(data => {
        let response = JSON.parse(JSON.stringify(data));
        if(response.length == 0){
          this.isLoadingResults = false;
          this.clientes = response;
          console.log("ZERO 1");
          console.log(this.clientes);
        return this.clientes;
        }
        this.clientes = response.data;
        console.log("ZERO 2");
        console.log(this.clientes);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

  deleteCliente(id) {
    this.isLoadingResults = true;
    this.api.deleteCliente(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.snackBar.open("Cliente Excluído com sucesso.",null, {
            duration : 2000,
          });
          this.router.navigate(['clientes']);
        }, (err) => {
          console.log(err);
          this.snackBar.open(err,null, {
            duration : 2000,
          });
          this.isLoadingResults = false;
        }
      );
  }
}