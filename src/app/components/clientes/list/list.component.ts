import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  clientes : any;
  columnsNames: string[] = [ 'RAZÃO SOCIAL', 'NOME FANTASIA', 'CNPJ','IE', 'CADASTRO', 'AÇÃO'];
  dataColumns: string[] = [ 'client_companyname', 'client_fantasyname', 'client_cnpj', 'client_ie', 'created_at','acao'];
  isLoadingResults = true;
  constructor(private _api: ApiService) { }
  ngOnInit() {
    this._api.getClients().subscribe(res => {
      let response = JSON.parse(JSON.stringify(res));
      this.clientes = response.data;
      this.isLoadingResults = false;
    }, err => {
      console.error(err);
       this.isLoadingResults = false;
    });
  }
  applyFilter(filterValue: string, formGroup : any) {
    formGroup.filter = filterValue.trim().toLowerCase();
  }
}