import { BrowserModule } from '@angular/platform-browser';
import { NgModule,LOCALE_ID  } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListComponent as ProductsListComponent } from './components/produtos/list/list.component';
import { ListComponent as OrgsListComponent } from './components/orgs/list/list.component';
import { ListComponent as ClientesListComponent } from './components/clientes/list/list.component';
import { ListComponent as UsersListComponent } from './components/users/list/list.component';
import { ListComponent as OrdersListComponent } from './components/orders/list/list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './menu/menu.component';
import {  
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSidenavModule,
  MatTableModule,
  MatToolbarModule,
  MatExpansionModule,
  MatGridListModule,
  MatRadioModule,  
  MatDatepickerModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatStepperModule,
  MatSnackBarModule,
  MatDialogModule,
  MatChipsModule, 
  MatAutocompleteModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';
import { DetailComponent } from './components/produtos/detail/detail.component';
import { DetailComponent as OrgsDetailComponent } from './components/orgs/detail/detail.component';
import { DetailComponent as ClientesDetailComponent } from './components/clientes/detail/detail.component';
import { DetailComponent as UsersDetailComponent } from './components/users/detail/detail.component';
import { DetailComponent as OrdersDetailComponent } from './components/orders/detail/detail.component';
import { CreateComponent as ProductNovo } from './components/produtos/create/create.component';
import { CreateComponent as LoteNovo } from './components/lotes/create/create.component';
import { EditComponent } from './components/produtos/edit/edit.component';
import { EditComponent as OrgsEditComponent } from './components/orgs/edit/edit.component';
import { EditComponent as ClientesEditComponent } from './components/clientes/edit/edit.component';
import { EditComponent as UsersEditComponent } from './components/users/edit/edit.component';
import { EditComponent as OrdersEditComponent } from './components/orders/edit/edit.component';
import localePtBr from '@angular/common/locales/pt';
import localePtBrExtra from '@angular/common/locales/extra/pt';
import { registerLocaleData } from '@angular/common';
import { MovtaComponent } from './components/produtos/movta/movta.component';
import { DeleteDialogComponent, DialogContentDelete } from './components/dialogs/delete-dialog/delete-dialog.component';
import { CreateComponent as OrgsCreateComponent } from './components/orgs/create/create.component';
import { CreateComponent as ClientesCreateComponent } from './components/clientes/create/create.component';
import { CreateComponent as UsersCreateComponent } from './components/users/create/create.component';
import { CreateComponent as OrdersCreateComponent } from './components/orders/create/create.component';
import { ModalComponent } from './components/dialogs/modal/modal.component';
import { DatatableComponent } from './widgets/lists/datatable/datatable.component';
import { InfoComponent } from './widgets/lists/info/info.component';
import { DataformComponent } from './widgets/forms/dataform/dataform.component';
import { DateagoPipe } from './pipes/dateago.pipe';
import { ServiceOrderComponent } from './components/orders/service-order/service-order.component';
import { LoginComponent } from './components/login/login.component';
import { DragDropModule } from '@angular/cdk/drag-drop';

registerLocaleData(localePtBr, localePtBrExtra);

@NgModule({
  declarations: [
    AppComponent,
    ProductsListComponent,
    OrgsListComponent,
    ClientesListComponent,
    UsersListComponent,
    OrdersListComponent,
    MenuComponent,
    DetailComponent,
    OrgsDetailComponent,
    ClientesDetailComponent,
    UsersDetailComponent,
    OrdersDetailComponent,
    ProductNovo,
    LoteNovo,
    EditComponent,
    OrgsEditComponent,
    ClientesEditComponent,
    UsersEditComponent,
    OrdersEditComponent,
    MovtaComponent,
    DeleteDialogComponent,
    DialogContentDelete,
    OrgsCreateComponent,
    ClientesCreateComponent,
    UsersCreateComponent,
    OrdersCreateComponent,
    ModalComponent,
    DatatableComponent,
    InfoComponent,
    DataformComponent,
    DateagoPipe,
    ServiceOrderComponent,
    LoginComponent
  ],
  entryComponents: [
    DeleteDialogComponent,
    DatatableComponent,
    InfoComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,  
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatListModule,
    MatProgressSpinnerModule, 
    MatSelectModule,
    MatSidenavModule,  
    MatTableModule,
    MatToolbarModule,
    MatExpansionModule,
    MatGridListModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatStepperModule,
    MatSnackBarModule,
    MatDialogModule,
    MatChipsModule,
    MatAutocompleteModule,
    DragDropModule,
    LayoutModule
  ],
  providers: [{provide: LOCALE_ID, useValue: 'pt-BR'}, DeleteDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
