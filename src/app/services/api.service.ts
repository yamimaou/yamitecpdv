import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Produto } from 'src/model/produto';
import { Category } from 'src/model/category';
import { Lote } from 'src/model/lote';

let _token = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')).token : 'null';
/*eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyQXV0aCI6eyJ1c2VyX2lkIjoxLCJjbGllbnRfaWQiOjEsInVzZXJfbmFtZSI6ImFkbWluIiwidXNlcl9lbWFpbCI6ImFkbWluQHlhbWl0ZWMuY29tIiwidXNlcl9wd2QiOiI2MGQzNjg2NDU5MTFjNTViNDQ5ZGFlZmE2NDYzYjE5MWNiNzA1MWIxIiwidXNlcl9zdGF0dXMiOjEsInVzZXJfdHlwZSI6MSwidXNlcl9wcm9maWxlIjoxLCJ1c2VyX3Rva2VuIjpudWxsLCJjcmVhdGVkX2F0IjoiMjAxOS0wOS0yNVQwMDozNDozOS4wMDBaIiwidXBkYXRlZF9hdCI6bnVsbCwibGFzdF9zZWVuIjoiMjAxOS0xMC0wMVQxNDoxNzo0NC4wMDBaIn0sImlhdCI6MTU3MTA4MjkyOSwiZXhwIjoxNjAyNjE4OTI5fQ.uyOJSBMDyhEolvRR962JLzh0ftM827htlZmkkSLCoGw';*/
const Header = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const tokenHeaders = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'x-access-token': _token })
};
//const apiUrl = 'http://yamitec.com:3000/api/';
const apiUrl = 'http://localhost:3000/api/';

@Injectable({

  providedIn: 'root'
})
export class ApiService {
  products = [];
  constructor(private http: HttpClient) {
    _token = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')).token : 'null';
  }
  /** AUTH */
  getToken(form): Observable<Element[]> {
    console.log(form);
    return this.http.post<Element[]>(apiUrl + 'auth', form, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((auth: Element[]) => console.log(`adicionou a Organização com w/ id=${auth['data']}`)),
      catchError(this.handleError<Element[]>('getToken'))
    );
  }

  set1Token() {
    this.http.post(apiUrl + 'auth', { user: "admin", pwd: "cartola" }, Header)
      .subscribe(res => {
        return JSON.parse(JSON.stringify(res)).token;
      });
    // .map(response=> response.json())
  }
  get1Token() {
    console.log(_token);
    return _token;
  }

  /** ORGANIZAÇÕES */
  getOrgs(): Observable<Element[]> {
    return this.http.get<Element[]>(apiUrl + 'orgs', tokenHeaders)
      .pipe(
        tap(orgs => console.log('leu os Organizacoes')),
        catchError(this.handleError('getOrgs', []))
      );
  }
  getOrg(id: number): Observable<Element[]> {
    const url = `${apiUrl}orgs/${id}`;
    return this.http.get<Element[]>(url, tokenHeaders).pipe(
      tap(_ => console.log(`leu a organização id=${id}`)),
      catchError(this.handleError<Element[]>(`getOrg id=${id}`))
    );
  }
  addOrg(org): Observable<Element[]> {
    return this.http.post<Element[]>(apiUrl + 'orgs', org, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((org: Element[]) => console.log(`adicionou a Organização com w/ id=${org['org_id']}`)),
      catchError(this.handleError<Element[]>('adOrgs'))
    );
  }
  updateOrg(id, org): Observable<any> {
    const url = `${apiUrl}orgs/${id}`;
    return this.http.put(url, org, tokenHeaders).pipe(
      tap(_ => console.log(`atualiza a org com id=${id}`)),
      catchError(this.handleError<any>('updateOrg'))
    );
  }
  deleteOrg(id): Observable<Element[]> {
    const url = `${apiUrl}orgs/${id}`;
    return this.http.delete<Element[]>(url, tokenHeaders).pipe(
      tap(_ => console.log(`remove a organizaçao com id=${id}`)),
      catchError(this.handleError<Element[]>('deleteOrg'))
    );
  }
  /*** CLIENTES */
  getClients(): Observable<Element[]> {
    return this.http.get<Element[]>(apiUrl + 'clients', tokenHeaders)
      .pipe(
        tap(clients => console.log('leu os unities')),
        catchError(this.handleError('getClients', []))
      );
  }
  getClient(id: number): Observable<Element[]> {
    const url = `${apiUrl}clients/${id}`;
    return this.http.get<Element[]>(url, tokenHeaders).pipe(
      tap(_ => console.log(`leu o cliente id=${id}`)),
      catchError(this.handleError<Element[]>(`getClient id=${id}`))
    );
  }
  addCliente(cliente): Observable<Element[]> {
    return this.http.post<Element[]>(apiUrl + 'clients', cliente, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((client: Element[]) => console.log(`adicionou o Cliente com w/ id=${cliente['client_id']}`)),
      catchError(this.handleError<Element[]>('addClient'))
    );
  }
  updateCliente(id, cliente): Observable<any> {
    const url = `${apiUrl}clients/${id}`;
    return this.http.put(url, cliente, tokenHeaders).pipe(
      tap(_ => console.log(`atualiza o cliente com id=${id}`)),
      catchError(this.handleError<any>('updateOrg'))
    );
  }
  deleteCliente(id): Observable<Element[]> {
    const url = `${apiUrl}clients/${id}`;
    return this.http.delete<Element[]>(url, tokenHeaders).pipe(
      tap(_ => console.log(`remove o cliente com id=${id}`)),
      catchError(this.handleError<Element[]>('deleteOrg'))
    );
  }
  /** ENDEREÇOS */
  getEnderecos(): Observable<Element[]> {
    return this.http.get<Element[]>(apiUrl + 'addresses', tokenHeaders)
      .pipe(
        tap(addresses => console.log('leu os endereços')),
        catchError(this.handleError('getAddresses', []))
      );
  }
  addEndereco(endereco): Observable<Element[]> {
    return this.http.post<Element[]>(apiUrl + 'addresses', endereco, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((client: Element[]) => console.log(`adicionou o Endereço com w/ id=${endereco['address_id']}`)),
      catchError(this.handleError<Element[]>('addEndereco'))
    );
  }
  /** Unidade */
  addUnidade(unidade): Observable<Element[]> {
    return this.http.post<Element[]>(apiUrl + 'unities', unidade, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((client: Element[]) => console.log(`adicionou a Unidade com w/ id=${unidade['unity_id']}`)),
      catchError(this.handleError<Element[]>('addUnidade'))
    );
  }
  /** Taxas */
  addTaxas(taxas): Observable<Element[]> {
    return this.http.post<Element[]>(apiUrl + 'taxes', taxas, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((client: Element[]) => console.log(`adicionou os Impostos com w/ id=${taxas['taxes_id']}`)),
      catchError(this.handleError<Element[]>('addTaxas'))
    );
  }
  /*** CATEGORIAS */
  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(apiUrl + 'categories', tokenHeaders)
      .pipe(
        tap(categories => console.log('leu os categorias')),
        catchError(this.handleError('getCategories', []))
      );
  }
  /** PRODUTOS */
  getProdutos(): Observable<Produto[]> {
    return this.http.get<Produto[]>(apiUrl + 'products', tokenHeaders)
      .pipe(
        tap(produtos => console.log('leu os produtos')),
        catchError(this.handleError('getProdutos', []))
      );
  }
  getProduto(id: number): Observable<Produto> {
    const url = `${apiUrl}products/${id}`;
    return this.http.get<Produto>(url, tokenHeaders).pipe(
      tap(_ => console.log(`leu o produto id=${id}`)),
      catchError(this.handleError<Produto>(`getProduto id=${id}`))
    );
  }
  addProduto(produto): Observable<Produto> {
    return this.http.post<Produto>(apiUrl + 'products', produto, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((produto: Produto) => console.log(`adicionou o produto com w/ id=${produto.product_id}`)),
      catchError(this.handleError<Produto>('addProduto'))
    );
  }
  updateProduto(id, produto): Observable<any> {
    const url = `${apiUrl}products/${id}`;
    return this.http.put(url, produto, tokenHeaders).pipe(
      tap(_ => console.log(`atualiza o produco com id=${id}`)),
      catchError(this.handleError<any>('updateProduto'))
    );
  }
  deleteProduto(id): Observable<Produto> {
    const url = `${apiUrl}products/${id}`;

    return this.http.delete<Produto>(url, tokenHeaders).pipe(
      tap(_ => console.log(`remove o produto com id=${id}`)),
      catchError(this.handleError<Produto>('deleteProduto'))
    );
  }
  addMovta(movta): Observable<Element[]> {
    return this.http.post<Element[]>(apiUrl + 'movta', movta, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((movta: Element[]) => console.log(` movimentou com w/ id=${movta['productmovta_id']}`)),
      catchError(this.handleError<Element[]>('addLote'))
    );
  }
  addLote(lote): Observable<Lote> {
    return this.http.post<Lote>(apiUrl + 'lotes', lote, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((lote: Lote) => console.log(`adicionou o lote com w/ id=${lote.productitem_id}`)),
      catchError(this.handleError<Lote>('addLote'))
    );
  }
  /** SERIÇOS */
  getServicos(): Observable<Produto[]> {
    return this.http.get<Produto[]>(apiUrl + 'services', tokenHeaders)
      .pipe(
        tap(service => console.log('leu os produtos')),
        catchError(this.handleError('getProdutos', []))
      );
  }
  getServico(id: number): Observable<Produto> {
    const url = `${apiUrl}services/${id}`;
    return this.http.get<Produto>(url, tokenHeaders).pipe(
      tap(_ => console.log(`leu o produto id=${id}`)),
      catchError(this.handleError<Produto>(`getProduto id=${id}`))
    );
  }
  addServico(service): Observable<Element[]> {
    return this.http.post<Element[]>(apiUrl + 'services', service, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((service: any) => console.log(`adicionou o produto com w/ id=${service.service_id}`)),
      catchError(this.handleError<Element[]>('addService'))
    );
  }
  updateServico(id, service): Observable<any> {
    const url = `${apiUrl}services/${id}`;
    return this.http.put(url, service, tokenHeaders).pipe(
      tap(_ => console.log(`atualiza o produco com id=${id}`)),
      catchError(this.handleError<any>('updateService'))
    );
  }
  deleteServico(id): Observable<Produto> {
    const url = `${apiUrl}services/${id}`;

    return this.http.delete<Produto>(url, tokenHeaders).pipe(
      tap(_ => console.log(`remove o produto com id=${id}`)),
      catchError(this.handleError<Produto>('deleteProduto'))
    );
  }
  /*** USUÁRIOS */
  getUsers(): Observable<Element[]> {
    return this.http.get<Element[]>(apiUrl + 'users', tokenHeaders)
      .pipe(
        tap(clients => console.log('leu os unities')),
        catchError(this.handleError('getUsers', []))
      );
  }
  getUser(id: number): Observable<Element[]> {
    const url = `${apiUrl}users/${id}`;
    return this.http.get<Element[]>(url, tokenHeaders).pipe(
      tap(_ => console.log(`leu o cliente id=${id}`)),
      catchError(this.handleError<Element[]>(`getClient id=${id}`))
    );
  }
  addUser(users): Observable<Element[]> {
    return this.http.post<Element[]>(apiUrl + 'users', users, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((client: Element[]) => console.log(`adicionou o Cliente com w/ id=${users['user_id']}`)),
      catchError(this.handleError<Element[]>('addClient'))
    );
  }
  updateUser(id, cliente): Observable<any> {
    const url = `${apiUrl}users/${id}`;
    return this.http.put(url, cliente, tokenHeaders).pipe(
      tap(_ => console.log(`atualiza o cliente com id=${id}`)),
      catchError(this.handleError<any>('updateOrg'))
    );
  }
  deleteUser(id): Observable<Element[]> {
    const url = `${apiUrl}users/${id}`;
    return this.http.delete<Element[]>(url, tokenHeaders).pipe(
      tap(_ => console.log(`remove o usuario com id=${id}`)),
      catchError(this.handleError<Element[]>('deleteUser'))
    );
  }

  /*** USUÁRIOS */
  getOrders(): Observable<Element[]> {
    return this.http.get<Element[]>(apiUrl + 'orders', tokenHeaders)
      .pipe(
        tap(clients => console.log('leu os unities')),
        catchError(this.handleError('getUsers', []))
      );
  }
  getStatus(): Observable<Element[]> {
    const url = `${apiUrl}status`;
    return this.http.get<Element[]>(url, tokenHeaders).pipe(
      tap(_ => console.log(`leu o status `)),
      catchError(this.handleError<Element[]>(`getStatus `))
    );
  }
  getPriority(): Observable<Element[]> {
    const url = `${apiUrl}priority/`;
    return this.http.get<Element[]>(url, tokenHeaders).pipe(
      tap(_ => console.log(`leu prioridades`)),
      catchError(this.handleError<Element[]>(`getPriority`))
    );
  }

  getOrder(id: number): Observable<Element[]> {
    const url = `${apiUrl}orders/${id}`;
    return this.http.get<Element[]>(url, tokenHeaders).pipe(
      tap(_ => console.log(`leu o cliente id=${id}`)),
      catchError(this.handleError<Element[]>(`getClient id=${id}`))
    );
  }
  addOrder(order): Observable<Element[]> {
    return this.http.post<Element[]>(apiUrl + 'orders', order, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((order: Element[]) => console.log(`adicionou o Cliente com w/ id=${order['order_id']}`)),
      catchError(this.handleError<Element[]>('addOrder'))
    );
  }
  addOrderItens(id,order): Observable<Element[]> {
    return this.http.post<Element[]>(apiUrl + `orders/additem/${id}`, order, tokenHeaders).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((order: Element[]) => console.log(`adicionou o Cliente com w/ id=${order['order_id']}`)),
      catchError(this.handleError<Element[]>('addOrder'))
    );
  }
  updateOrder(id, order): Observable<any> {
    const url = `${apiUrl}orders/${id}`;
    return this.http.put(url, order, tokenHeaders).pipe(
      tap(_ => console.log(`atualiza o order com id=${id}`)),
      catchError(this.handleError<any>('updateOrg'))
    );
  }
  deleteOrder(id): Observable<Element[]> {
    const url = `${apiUrl}orders/${id}`;
    return this.http.delete<Element[]>(url, tokenHeaders).pipe(
      tap(_ => console.log(`remove o usuario com id=${id}`)),
      catchError(this.handleError<Element[]>('deleteUser'))
    );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      localStorage.removeItem('userData');
      window.location.href = '/auth';
      return of(result as T);
    };
  }
}