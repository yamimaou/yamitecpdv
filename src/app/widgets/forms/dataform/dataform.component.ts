import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-dataform',
  templateUrl: './dataform.component.html',
  styleUrls: ['./dataform.component.css']
})
export class DataformComponent implements OnInit {
  produto: any;

  constructor(private fb: FormBuilder) { }
  fGroup = this.fb.group({
    created_at: [null]
  });
  //@Input() method: any = (param) => { console.log(param) };
  @Output() method = new EventEmitter();
  //@Output() selectClickAction = new EventEmitter();
  @Input() selectAction : any;
  @Input() campos = [
    {
      colname: 'Teste1',
      coltype: 'select', // text, hidden, date, option, select, textarea
      collabel: 'Campo teste 1',
      colvalue: 1,
      colvalidators: null,
      coldata: [
        { name: 'teste', value: 5 },
        { name: 'teste 1', value: 2 },
        { name: 'teste 2', value: 3 },
      ]
    },
  ];
  ngOnInit() {
    this.fGroup.removeControl("created_at");
    this.campos.forEach((v,k) => {
        this.fGroup.addControl(v.colname,new FormControl(v.colvalue, v.colvalidators ? v.colvalidators: null))
    })
    
  }
  selectClickAction(){
    this.selectAction()
  }
}