import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css']
})
export class DatatableComponent implements OnInit {
  @Input() _id: string = "id";
  @Input() dataUrl: string = "TESTE";
  @Input() acao: boolean = true;
  @Input() dataSource: any;
  @Input() columnsNames: string[] = ['NOME', 'DESCRIÇÃO', 'PREÇO'];
  @Input() dataColumns: string[] = ['product_name', 'product_description', 'product_price', 'acao']
  @Input() dataVals: any
  dataSources: any;
  isLoadingResults = true;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor() { }
  ngOnInit() {
    console.log(this.dataSource);
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.dataSources = new MatTableDataSource<Element[]>(this.dataSource)
      this.dataSources.paginator = this.paginator;
      this.dataSources.sort = this.sort;
    });
  }

  applyFilter(filterValue: string, formGroup: any) {
    formGroup.filter = filterValue.trim().toLowerCase();
  }
  isStrDateTime(str: any) {
    if (str  || str != null) {
      if(typeof str != "string") str = str.toString()
      if(str.split("-").length > 1) {
       // return true;
        if (str.toString().indexOf(":") > -1) {
          if (str.toString().substr(str.length - 1, str.length) == "Z") {
            return true
          }
        }
      }
    }
    false
  }
}