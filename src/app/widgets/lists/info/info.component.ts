import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { DeleteDialogComponent } from 'src/app/components/dialogs/delete-dialog/delete-dialog.component';
import { MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-widget-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})

export class InfoComponent implements OnInit {
  @Input() title: string = "id";
  @Input() description: string = "TESTE";
  @Input() dataSource: any;
  @Input() dataVer: any ;
  @Input() columnsNames: string[] = ['NOME', 'DESCRIÇÃO', 'PREÇO'];
  @Input() dataColumns: string[] = ['client_companyname', 'client_fantasyname', 'client_cnpj', 'client_ie', 'created_at'];
  @Input() editLink : string;
  @Input() routeId : string;
  @Output() deleteMethod = new EventEmitter();

  isLoadingResults = true;
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private deleteDialog: DeleteDialogComponent, public snackBar: MatSnackBar) { }

  ngOnInit() {
    console.log(this.dataVer)
  }
  isDate(str: string) {
    let ret = true;
    //let expReg = /^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$/;
    if (str == null) ret = false;
    else if (str.indexOf("-") == -1) ret = false;
    //else if (!expReg.test(str)) ret = false;
    else if (!!new Date(str).getTime())
      ret = !!new Date(str).getTime();
    console.log(str);
    console.log(!!new Date(str).getTime())
    return ret;
  }
  isStrDateTime(str : string = "2019-11-13T17:13:16.000Z") {
    if (str  || str != null) {
    if(str.split("-").length > 1) {
      if(str.split(":").length > 2) {
        if(str.substr(str.length -1,str.length) == "Z") {
            return true
        }
      }
    }
  }
    return false;

  }
}
