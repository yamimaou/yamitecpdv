import { Component } from '@angular/core';
import {
  transition,
  trigger,
  query,
  style,
  animate,
  group,
  animateChild,
  keyframes
} from '@angular/animations';
import { RouterOutlet } from '@angular/router';
import { slideInAnimation } from './route-animation';
import { ApiService } from './services/api.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('myAnimation', [
      transition('* => *', [
        query(
          ':enter',
          [style({ opacity: 0 })],
          { optional: true }
        ),
        query(':enter', [
          style({ transform: 'translateX(-100%)' }),
          animate('0.5s ease-in-out',
            style({ transform: 'translateX(0%)' }))
        ], { optional: true }),
        query(':leave', [
          style({ transform: 'translateX(0%)' }),
          animate('0.5s ease-in-out',
            style({ transform: 'translateX(100%)' }))
        ], { optional: true }),
      ])
    ])
  ]
})
export class AppComponent {
  title = 'yamitec-pdv';
  constructor(private _api:ApiService){}
  ngOnInit(){
    let actual_url = window.location.href.split('/');
    //alert(actual_url[actual_url.length-1].indexOf('auth'));
    //alert(localStorage.getItem('userData'))
    //alert(actual_url[actual_url.length-1].indexOf('auth'))
    if(!localStorage.getItem('userData') && actual_url[actual_url.length-1].indexOf('auth') != 0) {
      window.location.href = '/auth'
    }
    //localStorage.getItem('userData') && actual_url[actual_url.length-1].indexOf('auth') == -1 ? window.location.href = '/auth' : console.log('usuário logado');
  }
}
