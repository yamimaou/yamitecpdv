import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './components/produtos/list/list.component';
import { ListComponent as OrgsListComponent } from './components/orgs/list/list.component';
import { ListComponent as ClientesListComponent } from './components/clientes/list/list.component';
import { ListComponent as UsersListComponent } from './components/users/list/list.component';
import { ListComponent as OrdersListComponent } from './components/orders/list/list.component';
import { DetailComponent } from './components/produtos/detail/detail.component';
import { DetailComponent as OrgsDetailComponent} from './components/orgs/detail/detail.component';
import { DetailComponent as ClientesDetailComponent} from './components/clientes/detail/detail.component';
import { DetailComponent as UsersDetailComponent} from './components/users/detail/detail.component';
import { DetailComponent as OrdersDetailComponent} from './components/orders/detail/detail.component';
import { CreateComponent as ProductNovo } from './components/produtos/create/create.component';
import { CreateComponent as OrgsNovo } from './components/orgs/create/create.component';
import { CreateComponent as ClientesNovo } from './components/clientes/create/create.component';
import { CreateComponent as UsersNovo } from './components/users/create/create.component';
import { CreateComponent as OrdersNovo } from './components/orders/create/create.component';
import { EditComponent as ProductEditar } from './components/produtos/edit/edit.component';
import { EditComponent as OrgsEditar } from './components/orgs/edit/edit.component';
import { EditComponent as ClientesEditar } from './components/clientes/edit/edit.component';
import { EditComponent as UsersEditar } from './components/users/edit/edit.component';
import { EditComponent as OrdersEditar } from './components/orders/edit/edit.component';
import { CreateComponent as LoteNovo } from './components/lotes/create/create.component'; 
import {MovtaComponent} from './components/produtos/movta/movta.component';
import { LoginComponent } from './components/login/login.component';
import { ServiceOrderComponent } from './components/orders/service-order/service-order.component';


const routes: Routes = [
  /*** AUTH */
  {
    path: 'auth',
    component: LoginComponent,
    data: { title: 'Entrar', animation : '*' }
  },
  /*** PRODUTOS */
  {
    path: 'produtos',
    component: ListComponent,
    data: { title: 'Lista de Produtos', animation : '*' }
  },
  { path: '',
    redirectTo: '/auth',
    pathMatch: 'full'
  },
  {
    path: 'produtos/:id',
    component: DetailComponent,
    data: { title: 'Detalhe do Produto', animation : '*' }
  },
  {
    path: 'produto-novo',
    component: ProductNovo,
    data: { title: 'Adicionar Produto', animation : '*' }
  },
  {
    path: 'produto-editar/:id',
    component: ProductEditar,
    data: { title: 'Editar Produto', animation : '*' }
  },
  {
    path: 'lote-novo/:id',
    component: LoteNovo,
    data: { title: 'Adicionar Lote', animation : '*' }
  },
  {
    path: 'produto-movta/:id',
    component: MovtaComponent,
    data: { title: 'Movimentar Produto', animation : '*' }
  },
  /*** ORGANIZAÇÕES */
  { 
    path: 'organizacoes',
    component: OrgsListComponent,
    data: { title: 'Lista de organizacoes', animation : '*' }
  },
  {
    path: 'organizacoes/:id',
    component: OrgsDetailComponent,
    data: { title: 'Lista de Organizações', animation : '*' }
  },
  {
    path: 'organizacoes/:id',
    component: ServiceOrderComponent,
    data: { title: 'Gerenciamento do Técnico', animation : '*' }
  },
  {
    path: 'organizacao-novo',
    component: OrgsNovo,
    data: { title: 'Adicionar Organização', animation : '*' }
  },
  {
    path: 'orgs-editar/:id',
    component: OrgsEditar,
    data: { title: 'Editar Organização', animation : '*' }
  },
  /*** CLIENTES */
  {
    path: 'clientes',
    component: ClientesListComponent,
    data: { title: 'Lista de Clientes', animation : '*' }
  },
  {
    path: 'clientes/:id',
    component: ClientesDetailComponent,
    data: { title: 'Lista de Clientes', animation : '*' }
  },
  {
    path: 'cliente-novo',
    component: ClientesNovo,
    data: { title: 'Adicionar Cliente', animation : '*' }
  },
  {
    path: 'cliente-editar/:id',
    component: ClientesEditar,
    data: { title: 'Editar Cliente', animation : '*' }
  },
  /*** Usuarios */
  {
    path: 'usuarios',
    component: UsersListComponent,
    data: { title: 'Lista de usuarios', animation : '*' }
  },
  {
    path: 'usuarios/:id',
    component: UsersDetailComponent,
    data: { title: 'detalhes do usuarios', animation : '*' }
  },
  {
    path: 'usuario-novo',
    component: UsersNovo,
    data: { title: 'Adicionar usuario', animation : '*' }
  },
  {
    path: 'usuario-editar/:id',
    component: UsersEditar,
    data: { title: 'Editar usuario', animation : '*' }
  },
  /*** OS */
  {
    path: 'ordens',
    component: OrdersListComponent,
    data: { title: 'Lista de Ordens de Serviço', animation : '*' }
  },
  {
    path: 'ordens/:id',
    component: OrdersDetailComponent,
    data: { title: 'detalhes da Ordem de Serviço', animation : '*' }
  },
  {
    path: 'ordens/working/:id',
    component: ServiceOrderComponent,
    data: { title: 'Executando Serviço', animation : '*' }
  },
  {
    path: 'ordens-novo',
    component: OrdersNovo,
    data: { title: 'Adicionar Ordem de serviço', animation : '*' }
  },
  {
    path: 'ordens-editar/:id',
    component: OrdersEditar,
    data: { title: 'Editar Ordem de serviço', animation : '*' }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
