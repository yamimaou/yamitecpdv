import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'dateago',
    pure: true
})
export class DateagoPipe implements PipeTransform {
    now = moment(); // add this 2 of 4
    dateagomsg = " .. .";
    transform(value: any, args?: any): any {
        if (value) {
            console.log("MOMENT : " + this.now.diff(value, "months"))
            
            if (this.now.diff(value, "days") < 8) {
                this.dateagomsg = this.now.diff(value, "days") + " dia" + (this.now.diff(value, "days") > 1) ? "s" : "" + " atrás ";
            }
            if (this.now.diff(value, "weeks") < 5) {
                this.dateagomsg = this.now.diff(value, "weeks") + " semana" + (this.now.diff(value, "weeks") > 1) ? "s" : "" + " atrás ";
            }
            if (this.now.diff(value, "months") < 12) {
                this.dateagomsg = this.now.diff(value, "months") + " mês" + (this.now.diff(value, "months") > 1) ? "ses" : "" + " atrás ";
            }
            if (this.now.diff(value, "years") > 0) {
                this.dateagomsg = this.now.diff(value, "years") + " ano" + (this.now.diff(value, "years") > 1) ? "s" : "" + " atrás ";
            }
            const seconds = Math.floor((+new Date() - +new Date(value)) / 1000);
            if (seconds < 29) // less than 30 seconds ago will show as 'Just now'
                return 'Agora';
            const intervals = {
                'ano': 31536000,
                'mês': 2592000,
                'semana': 604800,
                'dia': 86400,
                'hora': 3600,
                'minuto': 60,
                'segundo': 1
            };
            let counter;
            for (const i in intervals) {
                counter = Math.floor(seconds / intervals[i]);
                if (counter > 0)
                    if (counter === 1) {
                        return counter + ' ' + i + ' atrás'; // singular (1 day ago)
                    } else {
                        return i.indexOf('mês') > -1 ? counter + ' ' + i + 'es atrás' : counter + ' ' + i + 's atrás'
                        //return counter + ' ' + i + 's atrás'; // plural (2 days ago)
                    }
            }
        }
        return this.dateagomsg;
    }

}